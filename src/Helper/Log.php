<?php declare(strict_types=1);

namespace Nextag\Checkout\Helper;

class Log
{
    protected $log = null;

    public function __construct(\Shopware\Core\System\SystemConfig\SystemConfigService $config)
    {
        $this->log = $config->get("NextagCheckout.config.logFilepath");
    }

    public function info($log_msg)
    {
        echo $log_msg."\n";
        $timestamp = date('Y-m-d_G-i-s');
        if ($this->log) {
            file_put_contents($this->log, $timestamp.": ".$log_msg . "\n", FILE_APPEND);
        }
    }

    public function debug($log_msg)
    {
        if (getenv("NEXTAG_DEBUGGER")) {
            echo $log_msg."\n";
            $timestamp = date('Y-m-d_G-i-s');
            if ($this->log) {
                file_put_contents($this->log, $timestamp.": ".$log_msg . "\n", FILE_APPEND);
            }
        }
    }

    public function error(\Exception $exception)
    {
        var_dump($exception);
        $timestamp = date('Y-m-d_G-i-s');
        if ($this->log) {
            file_put_contents($this->log, $timestamp.": ".$exception->getMessage() . "\n", FILE_APPEND);
        }
    }
}
