<?php declare(strict_types=1);

namespace Nextag\Checkout\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1587490632 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1587490632;
    }

    public function update(Connection $connection): void
    {
        $connection->executeQuery('
            CREATE TABLE IF NOT EXISTS `nextag_checkout_entity` (
                `id` BINARY(16) NOT NULL,
                `order_id` BINARY(16) NOT NULL,
                `comment` MEDIUMTEXT COLLATE utf8mb4_unicode_ci NULL,
                `commission` VARCHAR(255) COLLATE utf8mb4_unicode_ci NULL,
                `delivery_date` VARCHAR(10) COLLATE utf8mb4_unicode_ci NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.next_checkout.order_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
