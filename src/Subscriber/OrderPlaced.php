<?php declare(strict_types=1);

namespace Nextag\Checkout\Subscriber;

use Nextag\Checkout\Content\CheckoutService;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderPlaced implements EventSubscriberInterface
{
    private $requestStack;
    private $checkoutService;

    public function __construct(RequestStack $requestStack, CheckoutService $checkoutService)
    {
        $this->requestStack = $requestStack;
        $this->checkoutService = $checkoutService;
    }

    public static function getSubscribedEvents()
    {
        return [
            CheckoutOrderPlacedEvent::class => 'onOrderPlaced'
        ];
    }

    public function onOrderPlaced(CheckoutOrderPlacedEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $comment = $request->get('next_checkout_comment');
        $commission = $request->get('next_checkout_commission');
        $deliverydate = $request->get('date');
        $data = [];
        if ($comment && is_string($comment)) {
            $data['comment'] = $comment;
        }
        if ($commission && is_string($commission)) {
            $data['commission'] = $commission;
        }
        if ($deliverydate && is_string($deliverydate)) {
            $data['deliverydate'] = $deliverydate;
        }
        if (!empty($data)) {
          $this->checkoutService->save($data, $event);
        }
    }
}
