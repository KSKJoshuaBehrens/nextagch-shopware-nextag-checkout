<?php declare(strict_types=1);

namespace Nextag\Checkout\Content;

use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;

class CheckoutService
{
    /**
     * @var EntityRepositoryInterface
     */
    private $checkoutRepository;

    public function __construct(EntityRepositoryInterface $repository)
    {
        $this->checkoutRepository = $repository;
    }

    public function save(array $data, CheckoutOrderPlacedEvent $event): void
    {
        if (!empty($data)) {
            $data['orderId'] = $event->getOrder()->getId();
            $this->checkoutRepository->create([
                $data
            ], $event->getContext());
        }
    }
}
