<?php declare(strict_types=1);

namespace Nextag\Checkout\Content;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void                add(CheckoutEntity $entity)
 * @method void                set(string $key, CheckoutEntity $entity)
 * @method CheckoutEntity[]    getIterator()
 * @method CheckoutEntity[]    getElements()
 * @method CheckoutEntity|null get(string $key)
 * @method CheckoutEntity|null first()
 * @method CheckoutEntity|null last()
 */
class CheckoutCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return CheckoutEntity::class;
    }
}
