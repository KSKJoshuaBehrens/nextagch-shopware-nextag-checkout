<?php declare(strict_types=1);

namespace Nextag\Checkout\Content;

use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtensionInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Extension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OrderExtension implements EntityExtensionInterface
{
    public function extendFields(FieldCollection $fields): void
    {
        $fields->add(
            (new OneToOneAssociationField(
                'nextagCheckoutContentCheckout', 'id', 'order_id', CheckoutDefinition::class
            ))->addFlags(new Extension())
        );
    }

    public function getDefinitionClass(): string
    {
        return OrderDefinition::class;
    }
}
