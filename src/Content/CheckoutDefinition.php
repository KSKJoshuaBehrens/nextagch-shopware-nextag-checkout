<?php declare(strict_types=1);

namespace Nextag\Checkout\Content;

use Shopware\Core\Checkout\Order\OrderDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CheckoutDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'nextag_checkout_entity';
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField('order_id', 'orderId', OrderDefinition::class))->addFlags(new Required()),
            new LongTextField('comment', 'comment'),
            new StringField('commission', 'commission'),
            new StringField('delivery_date', 'deliveryDate'),
            new OneToOneAssociationField('order', 'order_id', 'id', OrderDefinition::class, false)
        ]);
    }

    public function getEntityClass(): string
    {
        return CheckoutEntity::class;
    }

    public function getCollectionClass(): string
    {
        return CheckoutCollection::class;
    }
}
