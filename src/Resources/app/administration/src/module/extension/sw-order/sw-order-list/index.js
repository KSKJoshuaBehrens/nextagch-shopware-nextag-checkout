import template from './sw-order-list.html.twig';
import './sw-order-list.scss';

Shopware.Component.override('sw-order-list', {
    template,

    computed: {
        orderColumns() {
            return this.expandColumns(this.$super('orderColumns'));
        }
    },

    methods: {
        expandColumns(columns) {         
            columns.push({
                label: this.$tc('checkout.commentTitle'),
                property: 'comment',
                allowResize: true
            });
            columns.push({
                label: this.$tc('checkout.commissionTitle'),
                property: 'commission',
                allowResize: true,
                align: 'left'
            });
            columns.push({
                label: this.$tc('checkout.deliveryDate'),
                property: 'deliverydate',
                allowResize: true,
                align: 'left'
            });

            return columns;
        }
    }
});
