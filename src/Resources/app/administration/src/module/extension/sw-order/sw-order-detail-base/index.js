import template from './sw-order-detail-base.html.twig';

Shopware.Component.override('sw-order-detail-base', {
    template,

    computed: {
        comment() {
            if ('nextagCheckoutContentCheckout' in this.order.extensions) {
                return this.order.extensions.nextagCheckoutContentCheckout.comment;
            }

            return null;
        },

        commission() {
            if ('nextagCheckoutContentCheckout' in this.order.extensions) {
                return this.order.extensions.nextagCheckoutContentCheckout.commission;
            }

            return null;
        },

        deliverydate() {
            if ('nextagCheckoutContentCheckout' in this.order.extensions) {
                return this.order.extensions.nextagCheckoutContentCheckout.deliverydate;
            }

            return null;
        }
    }
});
